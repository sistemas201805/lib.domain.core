### Estágio 1 - Obter o source e gerar o Build ###
FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS build
WORKDIR /app
COPY . ./
RUN dotnet restore ./Lib.Domain.Core.csproj
RUN dotnet build ./Lib.Domain.Core.csproj -c Release -o /app/out
