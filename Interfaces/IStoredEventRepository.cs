using System;
using System.Collections.Generic;
using Lib.Domain.Core.Interfaces;
using Lib.Domain.Core.Messages.Events;

namespace Lib.Infra.Data.Base.Repository.EventSourcing
{
    public interface IStoredEventRepository : IRepositoryEvent<StoredEvent>
    {
        IEnumerable<StoredEvent> ObterPorAggregateId(Guid aggregateId);
    }
}
