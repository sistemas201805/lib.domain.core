using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Lib.Domain.Core.Messages.Events;

namespace Lib.Domain.Core.Interfaces
{
    public interface IRepositoryEvent<TEntity> : IDisposable where TEntity : Event
    {
        void Adicionar(TEntity obj);
        TEntity ObterPorId(Guid id);
        IEnumerable<TEntity> ObterTodos();
        void Atualizar(TEntity obj);
        void Excluir(Guid id);
        IEnumerable<TEntity> Buscar(Expression<Func<TEntity, bool>> predicate);
        int SaveChanges();
    }
}
