using Lib.Domain.Core.Messages.Events;

namespace Lib.Domain.Core.Interfaces
{
    public interface IStoredEventController
    {
        void SalvarEvento<T>(T evento) where T : Event;
    }
}
