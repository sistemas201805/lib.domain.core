using System.Threading.Tasks;
using Lib.Domain.Core.Messages.Commands;
using Lib.Domain.Core.Messages.Events;

namespace Lib.Domain.Core.Interfaces
{
    public interface IMediatorHandler
    {
        Task PublicarEvento<T>(T evento) where T : Event;
        Task EnviarComando<T>(T comando) where T : Command;
    }
}
