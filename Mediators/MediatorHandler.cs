using System.Threading.Tasks;
using Lib.Domain.Core.Messages.Commands;
using Lib.Domain.Core.Messages.Events;
using Lib.Domain.Core.Interfaces;
using MediatR;

namespace Lib.Domain.Core.Mediators
{
    public class MediatorHandler : IMediatorHandler
    {
        private readonly IMediator _mediator;
        private readonly IStoredEventController _storedEvent;

        public MediatorHandler(IMediator mediator, IStoredEventController storedEvent)
        {
            _mediator = mediator;
            _storedEvent = storedEvent;
        }

        public async Task EnviarComando<T>(T comando) where T : Command
        {
            await _mediator.Send(comando);
        }

        public async Task PublicarEvento<T>(T evento) where T : Event
        {
            if (!evento.Acao.Equals("DomainNotification"))
                _storedEvent?.SalvarEvento(evento);

            await _mediator.Publish(evento);
        }
    }
}
