using System;

namespace Lib.Domain.Core.Messages
{
    public abstract class Message
    {
        public string Acao { get; protected set; }
        public Guid AggregateId { get; protected set; }

        protected Message()
        {
            Acao = GetType().Name;
        }
    }
}
