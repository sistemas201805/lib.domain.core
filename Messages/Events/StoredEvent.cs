using System;

namespace Lib.Domain.Core.Messages.Events
{
    public class StoredEvent : Event
    {
        public StoredEvent(Event evento, string data, string user)
        {
            Id = Guid.NewGuid();
            AggregateId = evento.AggregateId;
            Acao = evento.Acao;
            Data = data;
            User = user;
        }

        // EF Constructor 
        protected StoredEvent() { }

        public string Data { get; private set; }

        public string User { get; private set; }
    }
}
