using System;
using MediatR;

namespace Lib.Domain.Core.Messages.Events
{
    public abstract class Event : Message, INotification
    {
        public Guid Id { get; set; }
        public DateTime DataCriacao { get; set; }
        protected Event()
        {
            DataCriacao = DateTime.Now;
        }
    }
}
