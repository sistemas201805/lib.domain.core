using Lib.Domain.Core.Interfaces;
using Lib.Domain.Core.Notifications;
using MediatR;

namespace Lib.Domain.Core.Messages.Commands
{
    public abstract class CommandHandler
    {
        private readonly IUnitOfWork _uow;
        private readonly IMediatorHandler _mediator;
        private readonly DomainNotificationHandler _notifications;
        protected CommandHandler(IUnitOfWork uow, IMediatorHandler mediator, INotificationHandler<DomainNotification> notifications)
        {
            _uow = uow;
            _mediator = mediator;
            _notifications = (DomainNotificationHandler)notifications;
        }

        protected bool Commit()
        {
            if (_notifications.HasNotifications()) return false;

            if (_uow.Commit()) return true;

            _mediator.PublicarEvento(new DomainNotification("Commit", "Ocorreu um erro ao salvar no banco"));
            return false;
        }
    }
}
