using System;
using MediatR;

namespace Lib.Domain.Core.Messages.Commands
{
    public abstract class Command : Message, IRequest
    {
        public DateTime Timestamp { get; private set; }

        public Command()
        {
            Timestamp = DateTime.Now;
        }
    }
}
